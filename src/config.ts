require('dotenv').config();

const config = {
  PORT: Number(process.env.PORT),
  MONGO_URL: process.env.MONGO_URL!,
  SECRET_JWT: process.env.SECRET_JWT,
  URL: process.env.URL,
  SaltRounds:Number(process.env.SaltRounds)
};

export default config