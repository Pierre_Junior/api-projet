import express from 'express';
import mongoose from 'mongoose';
import config  from './config';
import product from './routes/product'
import db from './routes/db'
import auth from './routes/auth'
import { get404,getErrors } from './controller/errorController';


const app = express();
const PORT = config.PORT;
const MONGO_URL = config.MONGO_URL

// parse application/json
app.use(express.json());  



app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});


// Utilisation des routes en tant que middleware
app.use('/product',product)
app.use('/db',db)
app.use('/auth',auth)

// Gestion erreur 404
app.use(get404);

// Gestion des erreurs, ce middleware gère seulement les erreurs qui sont générées par le code
app.use(getErrors);


mongoose
  .connect(MONGO_URL)
  .then(() => {
    app.listen(PORT, () => {
      console.log('Node.js  est à l\'écoute sur le port %s ', PORT);
    });
  })
  .catch((err: any) => console.log(err));

  export default app

