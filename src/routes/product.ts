
import { Router } from "express";
import {createProduct, deleteProduct, deleteProducts, getProduct, getProducts, updateProduct} from '../controller/productController'
import { isAuth } from "../middleware/is-auth";

const router =Router()


router.get('/all',isAuth, getProducts );
router.post('/',isAuth, createProduct );
router.get('/:id',isAuth,getProduct)
router.put('/:id',isAuth,updateProduct)
router.delete('/:id',isAuth,deleteProduct)
router.delete('/all',isAuth,deleteProducts)


export default router


