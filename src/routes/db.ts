import { Router } from "express";
import { seed } from "../controller/dbController";

const router = Router();

router.get("/seed", seed );

export default router