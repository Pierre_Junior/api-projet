const userSeed = [
  {
    _id: "650dade494237ad07eedc7ee",
    username: "pierre",
    email: "user1@gare.ca",
    password: "$2a$10$5O.9Eep4lUyU3W9uMjsLuOrSgW/ebQQKiq9qX457nigHlMbExkyIC",
  },
  {
    _id: "650e08dc63d222e2c0afc396",
    username: "junior",
    email: "user2@test.ca",
    password: "$2a$10$5O.9Eep4lUyU3W9uMjsLuOrSgW/ebQQKiq9qX457nigHlMbExkyIC",
  },
];

export default userSeed;
