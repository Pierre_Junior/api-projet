import mongoose from "mongoose";

const Schema = mongoose.Schema;

const productSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, "The name is required"],
      unique: [true, "Same name is already exist"],
      maxlength: [50, "the name must have less than 50 character"],
      minlength: [
        3,
        "the name must have more than 3 character",
      ],
    },

    quantite: {
      type: Number,
      required: [true, "quantity is required"],
      validate: [
        {
          validator: Number.isInteger,
          message: "{VALUE} is not an integrer number",
        },
        {
          validator: function (value: number) {
            return value >= 0;
          },
          message: "quantity can not be less than zero",
        },
      ],
    },

    unit_price: {
      type: Number,
      required: [true, "unit price is required"],
      validate: [
        {
          validator: function (value: number) {
            return value >= 0;
          },
          message: "unit price can not be less than zero",
        },
      ],
    },

    deletedAt: {
      type: Date,
      default: null
    },

    user: {
        type: Schema.Types.ObjectId,
        required: [true, "user is required"],
        ref: 'User'
      }
  },
  { timestamps: true }
);

export default mongoose.model("Product", productSchema);
