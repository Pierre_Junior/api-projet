import mongoose from "mongoose";

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {

    email: {
      type: String,
      required: [true,"Courriel requis"],
      unique: [true,"Courriel déjà utilisé"],
      match: [/.+@.+\..+/, 'Adresse courriel invalide'],
      maxlength: [50, 'L\'adresse courriel doit contenir au plus 50 caractères']
    },
    username: {
      type: String,
      required: [true,"nom d'utilisateur requis"],
    },
    password: {
      type: String,
      required: [true,"mot de passe requis"],
      minlength: [6,"Le mot de passe doit contenir un minimum de 6 caractères"]
    },
    resetPasswordToken:{
      type:String,
      default:null
    }

  },
  { timestamps: true }
);


export default mongoose.model('User', userSchema);
