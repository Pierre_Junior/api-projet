import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import config from "../config";
import User from "../model/user";
import { NextFunction, Request, Response } from "express";
import { authService } from "../service/authService";

const autService = new authService();
const saltRounds = config.SaltRounds;

export async function login(req: Request, res: Response, next: NextFunction) {
  const { email, password } = req.body;
  try {
    let token = await autService.logIn(email, password);
    res.status(201).json({ token:token });
  } catch (error) {
    next(error);
  }
}


