import { NextFunction, Request, Response } from "express";
import {productService} from "../service/productService";

const prodService=new productService()

export async function  getProducts(req:Request, res:Response, next:NextFunction) 
{
  try {
    let product= await prodService.getProductsFunction()
    res.status(200).json(product);

  } catch (err) {
    next(err);
  }
};

export async function  getProduct(req:Request, res:Response, next:NextFunction) 
{
  try {
    const productId = req.params.id;
    let product= await prodService.getProductFunction(productId)
    
    res.status(200).json(product);

  } catch (err) {
    next(err);
  }
};

export async function  updateProduct(req:Request, res:Response, next:NextFunction) 
{
  try {
    const productId = req.params.id;

    const { name, quantite,unit_Price } = req.body;
    const userId= req.user.id
    let product= await prodService.updateProduct(productId,name,quantite,unit_Price,userId)
    
    res.status(200).json({
      product
    });

  } catch (err) {
    next(err);
  }
};

export async function  deleteProduct(req:Request, res:Response, next:NextFunction) 
{
  try {

    const productId = req.params.id;
    const userId= req.user.id
    await prodService.deleteProduct(productId,userId)
   
    
    res.status(204).send()

  } catch (err) {
    next(err);
  }
};

export async function  deleteProducts(req:Request, res:Response, next:NextFunction) 
{
  try {
    const userId= req.user.id
    await prodService.deleteProducts(userId)
    
    res.status(204).send()

  } catch (err) {
    next(err);
  }
};

export async function  createProduct(req:Request, res:Response, next:NextFunction) 
{
  try {
    const { name, quantite,unit_Price } = req.body;
    const userId= req.user.id
    const product=await prodService.createProduct(name,quantite,unit_Price,userId)
    
    res.status(201).json({
      message: "Produit créé avec succès!",
    });

  } catch (err) {
    next(err);
  }
};