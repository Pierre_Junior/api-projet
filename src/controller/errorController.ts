import { NextFunction, Request, Response } from "express";
import { Error } from "mongoose";

export function get404 (req :Request, res:Response, next:NextFunction){
    res.status(404).json('Requête introuvable !' );
  };
  
export function getErrors(err:any, req:Request, res:Response, next:NextFunction) {    
    // Si l'erreur est de type CastError, c'est qu'on a tenté de convertir un id invalide
    if (err.kind === 'ObjectId' && err.name === 'CastError') {
      err.statusCode = 400;
      err.message= `L'id n'existe pas`;
    }

    // Si l'erreur est de type ValidationError, c'est qu'on a tenté de créer une ressource invalide
    if (err.name === 'ValidationError') {
        err.message= `${err.message}`,
        err.statusCode = 400;
    }
  
    if (!err.statusCode) err.statusCode = 500;
    res.status(err.statusCode).json({ message: err.message, statusCode: err.statusCode });
  }