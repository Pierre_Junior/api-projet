import user from "../model/user";
import product from "../model/product";

import userSeed from "../seeds/users";
import productSeed from "../seeds/products";
import { NextFunction, Request, Response } from "express";

export async function seed (req:Request, res:Response, next:NextFunction) {
  try {
    await Promise.all([
      user.deleteMany(),
      product.deleteMany(),
    ]);

    const [usersInsert, productInsert] = await Promise.all([
      user.insertMany(userSeed),
      product.insertMany(productSeed),
    ]);
    

    res.status(200).json({usersInsert,productInsert});
  } catch (err) {
    next(err);
  }
};