import HttpError from "../Custom_class/httpError";
import product from "../model/product";

export class productService {
  async getProductsFunction() {
    const products = await product.find({ deletedAt: null }).populate({
      path: "user",
      select: "username email",
    });

    return products;
  }

  async getProductFunction(productId: string) {
    const prod = await product.findById(productId);

    if (!prod) {
      const error = new HttpError("Le produit n'existe pas.", 404);
      error.statusCode = 404;
      throw error;
    }

    return prod;
  }

  async updateProduct(productId: string, name:string, quantite:Number, unit_price:Number, userId:string) {
    
    const prod= await product.findById(productId)

    if (!prod) {
      throw new HttpError("Aucun produit trouvé pour la mise à jour !",404)
    }
  
    if(prod.user._id.toString()!=userId){
      throw new HttpError("Vous n'avez pas le droit de modifier la ressource", 403)
    }

    const updateData={name,quantite,unit_price}
    const updatedProd = await product.findByIdAndUpdate(productId, updateData, {
      new: true,
      runValidators: true,
    });

    return updatedProd;

  }

  async deleteProduct(productId: string,userId:string) {
    
    const prod= await product.findById(productId)

    if (!prod) {
      throw new HttpError("Aucun produit trouvé pour la mise à jour !",404)
    }
    if(prod.user._id.toString()!=userId){
      throw new HttpError("Vous n'avez pas le droit de supprimer la ressource", 403)
    }

    await product.findByIdAndUpdate(productId, {deletedAt: new Date()}, {
      new: true,
      runValidators: true,
    });

  }

  async deleteProducts(userId:string) {
    
    await product.updateMany(
      {user:userId},
      {$set:{deletedAt: new Date()}}
    );


  }

  async createProduct(name:string, quantite:Number, unit_price:Number, userId:string) {

    const produit = new product({
      name: name,
      quantite: quantite,
      unit_price:unit_price,
      user:userId
    });

    const result = await produit.save();

    return result;
    

  }
}
