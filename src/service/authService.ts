import User from "../model/user";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import config from "../config";
import HttpError from "../Custom_class/httpError";

export class authService {
  async logIn(email: string, password: string) {
    //chercher un utilisateur à fonction de son email
    const utilisateurTrouve = await User.findOne({ email: email });
    let tokenn;
    //Si un utilisateur est trouvé
    if (utilisateurTrouve) {
      await bcrypt
        .compare(password, utilisateurTrouve.password)
        .then((result) => {
          if (result) {
            const token = jwt.sign(
              {
                id: utilisateurTrouve.id,
              },
              config.SECRET_JWT!,
              { expiresIn: "1h" }
            );
            tokenn=token
          }
        });
    }
    if(tokenn)
      return tokenn;
    else
      throw new HttpError("Courriel ou mot de passe invalide !", 400);
  }
}
