
import jwt from 'jsonwebtoken'
import config  from '../config';
import { NextFunction, Request, Response } from 'express';
import HttpError from '../Custom_class/httpError';

const SECRET_JWT = config.SECRET_JWT;

// Étendre l'interface Request pour inclure la propriété user
declare global {
  namespace Express {
    interface Request {
      user?: any;
    }
  }
}


/** Vérifie si la requête a un token JWT valide */
export function isAuth(req:Request, res:Response, next:NextFunction){
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    res.status(401).send({ error: 'Non authentifié..' });
  }
  const token = authHeader!.split(' ')[1];

  let decodedToken;
  try {
    decodedToken = jwt.verify(token, SECRET_JWT!);

  } catch (err:any) {
    err.statusCode = 401;
    throw err;
  }
  if (!decodedToken) {
    const error = new HttpError('Non authentifié.',401);
    throw error;
  }
  req.user = decodedToken;
  next();
};
