class HttpError extends Error {
    statusCode: number;
  
    constructor(message: string, statusCode: number) {
      super(message);
      this.statusCode = statusCode;
      // Maintenir la trame de la pile correcte pour les endroits où cette erreur a été lancée (uniquement disponible en V8)
      Error.captureStackTrace(this, this.constructor);
    }
  }
  
  export default HttpError;